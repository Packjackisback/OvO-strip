1: Run git clone https://gitlab.com/ovo1/OvO-path.git
2: Rename .ovo replay file to .json
3: Run ovo.py
4: Enter the path to the .json file
5: Enter the level (JUST THE 1-2 NUMBERS. EX: 4, or 45)
6: Enter the time (JUST THE 6 NUMBERS. EX 006734)
7: It will now strip the file and export it to onlycords.txt 
8: proceed to use it with desmos



Note: I will eventually update to automatically graph it, but for now this is what I've got.
